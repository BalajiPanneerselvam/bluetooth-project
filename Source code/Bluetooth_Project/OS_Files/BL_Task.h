#ifndef BL_TASK_H
#define BL_TASK_H
/***********************************************************************************************************************/
/* DISCLAIMER                                                                                                          */
/*                                                                                                                     */
/* Copyright (C) 2011, 2016 Niyata Infotech Ptv. Ltd. All rights reserved.                                             */
/***********************************************************************************************************************/
/*                                               F I L E  D E S C R I P T I O N                                        */
/*---------------------------------------------------------------------------------------------------------------------*/
/*File name                       :BL_Task.h                                                                                    */
/*Version                         :V1.0                                                                                    */
/*Micros supported                :TMS570LS1224                                                                                    */
/*Compilers supported             :GCC(CCS IDE)                                                                                    */
/*Platforms supported             :Hercules                                                                                    */
/*Description                     :Bluetooth Task header file                                                                                     */
/*                                :                                                                                    */
/*                                :                                                                                    */
/*---------------------------------------------------------------------------------------------------------------------*/
/**********************************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/**********************************************************************************************************************/
/*                                                 F I L E  V E R S I O N                                             */
/**********************************************************************************************************************/
#define BL_TASK_H_VERSION  0x0100    /* BCD values - MSB & LSB represent major & minor versions respectively \
                                        00.00 to 99.99 */

/**********************************************************************************************************************/
/*                                               I N C L U D E   F I L E S                                            */
/**********************************************************************************************************************/
#include "BL_Task.h"

/**********************************************************************************************************************/
/*                                      V E R S I O N   I N T E G R I T Y  C H E C K                                  */
/**********************************************************************************************************************/
/* perform integrity check when it is necessary */
#if ( BL_TASK_H_VERSION != 0x0100 )
    #error "Incorrect version of header file used in BL_Task.h!!!"
#endif

/**********************************************************************************************************************/
/*                                          M A C R O   D E F I N I T I O N S                                         */
/**********************************************************************************************************************/
/*************************************Bluetooth Command Macros ********************************************************/
#define BL_CONNECT                0x30U  //0
#define BL_ENABLE                 0x31U  //1
#define BL_DISABLE                0x32U  //2
#define BL_VERSION_REQUEST        0x33U  //3
#define BL_SERINAL_NUMBER_REQUEST 0x34U  //4
/**********************************************************************************************************************/

#define BL_TASK_FIRMWARE_V_LENGTH 0x0AU
#define BL_PERMIT                 0x01U
#define BL_DENIED                 0x00U


#define BL_TASK_COMMAND_NUMBER            0x05U
#define BL_TASK_COMMAND_RESPONSE          0x09U

/* BL Command Response Length */
#define BL_CONNECT_RESPONSE_LENGTH        0x01U
#define BL_ENABLE_RESPONSE_LENGTH         0x01U
#define BL_DISABLE_RESPONSE_LENGTH        0x01U
#define BL_VERSION_RESPONSE_LENGTH        0x08U
#define BL_SERIALNUMBER_RESPONSE_LENGTH   0x08U

/*BL Response Macros */

#define BL_CONNECT_RESPONSE               0x11U
#define BL_ENABLE_RESPONSE                0x12U
#define BL_DISABLE_RESPONSE               0x13U
#define BL_VERSION_RESPONSE_B1            'B'
#define BL_VERSION_RESPONSE_B2            'L'
#define BL_VERSION_RESPONSE_B3            'A'
#define BL_VERSION_RESPONSE_B4            'P'
#define BL_VERSION_RESPONSE_B5            'V'
#define BL_VERSION_RESPONSE_B6            '1'
#define BL_VERSION_RESPONSE_B7            '.'
#define BL_VERSION_RESPONSE_B8            '0'

#define BL_SERIALNUMBER_RESPONSE_B1       0x01U
#define BL_SERIALNUMBER_RESPONSE_B2       0x02U
#define BL_SERIALNUMBER_RESPONSE_B3       0x02U
#define BL_SERIALNUMBER_RESPONSE_B4       0x00U
#define BL_SERIALNUMBER_RESPONSE_B5       0x00U
#define BL_SERIALNUMBER_RESPONSE_B6       0x00U
#define BL_SERIALNUMBER_RESPONSE_B7       0x00U
#define BL_SERIALNUMBER_RESPONSE_B8       0x00U

/**********************************************************************************************************************/
/*                                          T Y P E   D E F I N I T I O N S                                           */
/**********************************************************************************************************************/

typedef enum BL_States
{
    BL_Idle_State    = 0x00U,
    BL_Connect_State = 0x01U,
    BL_Enable_State  = 0x02U,
    BL_Disable_State = 0x03U,
    BL_Request_State = 0x04U
}BL_States_t;

typedef unsigned char BL_Access_flg_t;

typedef unsigned char uint8_t;
/**********************************************************************************************************************/
/*                                     G L O B A L   V A R I A B L E S   D E C L A R A T I O N                        */
/**********************************************************************************************************************/

static const uint8_t Bluetooth_App_Command_Table[BL_TASK_COMMAND_NUMBER][BL_TASK_COMMAND_RESPONSE] = \
{
    BL_CONNECT_RESPONSE_LENGTH,BL_CONNECT_RESPONSE,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U, \
    BL_ENABLE_RESPONSE_LENGTH,BL_ENABLE_RESPONSE,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U, \
    BL_DISABLE_RESPONSE_LENGTH,BL_DISABLE_RESPONSE,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U, \
    BL_VERSION_RESPONSE_LENGTH,BL_VERSION_RESPONSE_B1,BL_VERSION_RESPONSE_B2,BL_VERSION_RESPONSE_B3,BL_VERSION_RESPONSE_B4,BL_VERSION_RESPONSE_B5,BL_VERSION_RESPONSE_B6,BL_VERSION_RESPONSE_B7,BL_VERSION_RESPONSE_B8, \
    BL_SERIALNUMBER_RESPONSE_LENGTH,BL_SERIALNUMBER_RESPONSE_B1,BL_SERIALNUMBER_RESPONSE_B2,BL_SERIALNUMBER_RESPONSE_B3,BL_SERIALNUMBER_RESPONSE_B4,BL_SERIALNUMBER_RESPONSE_B5,BL_SERIALNUMBER_RESPONSE_B6,BL_SERIALNUMBER_RESPONSE_B7,BL_SERIALNUMBER_RESPONSE_B8 \

};



/**********************************************************************************************************************/
/*                                    P U B L I C     F U N C T I O N S    D E C L A R A T I O N                      */
/**********************************************************************************************************************/

void BL_Task_Send_Response(uint8_t rx_command);

void BL_Callback(uint8_t Rx_data);

void BL_Bg_Task(void);

void BL_Task_init(void);

void BL_Switch_Task(void);
/**********************************************************************************************************************/
/*                                              E N D   O F   S O F T W A R E                                         */
/**********************************************************************************************************************/
#ifdef __cplusplus
}
#endif
/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                              R E V I S I O N   H I S T O R Y                                       */
/*                                                                                                                    */
/***********************************************************************************************************************
    REVISION NUMBER      : V1.0
    REVISION DATE        : 11 - 06 - 17
    CREATED / REVISED BY : Balaji
    DESCRIPTION          : Initial version
------------------------------------------------------------------------------------------------------------------------

***********************************************************************************************************************/
#endif
