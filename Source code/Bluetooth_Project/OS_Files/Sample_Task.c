/*
 * Sample_Task.c
 *
 *  Created on: Nov 6, 2017
 *      Author: Roop
 */
#include "gio.h"
#include "sci.h"
#include "Sample_Task.h"


void Sample_rr_task(void)
{




}


void Sample_Init(void)
{




}


void Sample_tt_task1(void)
{


    gioToggleBit(gioPORTA, 0);


}

void Sample_tt_task2(void)
{

    gioToggleBit(gioPORTA, 1);

}

void Sample_tt_task3(void)
{

    gioToggleBit(gioPORTA, 0);

}

void Sample_bltask(uint8_t Rx_data)
{

 switch(Rx_data)
 {
     case 0x30:

         Sample_BlService(0x01);
     break;
     case 0x31:

         Sample_BlRequest();
     break;

     case 0x32:

         Sample_BlService(0x00);

     default:

     break;

 }

}


void Sample_BlService(uint8_t val)
{

    gioSetBit(gioPORTB, 1, val);
    gioSetBit(gioPORTB, 2, val);

}

void Sample_BlRequest(void)
{

    uint8_t bit_value[2]={0,0x0D};

    bit_value[0] = (uint8_t) gioGetPort(gioPORTB);

    sciSend(sciREG,2, bit_value);
}

