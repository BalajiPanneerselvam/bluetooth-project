/***********************************************************************************************************************
* DISCLAIMER
*
* Copyright (C) 2011, 2016 Niyata Infotech Ptv. Ltd. All rights reserved.
***********************************************************************************************************************/

/*********************************************************************************************************************************/
/*                                               F I L E  D E S C R I P T I O N                                                  */
/*-------------------------------------------------------------------------------------------------------------------------------*/
/*File name                       :os_main.c                                                                                     */
/*Version                         :V1.0                                                                                          */
/*Micros supported                :TMS570LS1224,                                                                                 */
/*Compilers supported             :CCS                                                                                           */
/*Platforms supported             :Hercules                                                                                      */
/*Description                     :                                                                                              */
/*-------------------------------------------------------------------------------------------------------------------------------*/
/*********************************************************************************************************************************/

/*********************************************************************************************************************************/
/*                                                 F I L E  V E R S I O N                                             */
/*********************************************************************************************************************************/
#define OS_MAIN_C_VERSION  0x0100U    /* BCD values - MSB & LSB represent major & minor versions respectively */
                                  /*00.00 to 99.99 */

/*********************************************************************************************************************************/
/*                                               I N C L U D E   F I L E S                                            */
/*********************************************************************************************************************************/

#include "os_main.h"
#include "os.h"
#include "os_config.h"
#include "os_types.h"
#include "rti.h"

/*********************************************************************************************************************************/
/*                                      V E R S I O N   I N T E G R I T Y  C H E C K                                  */
/*********************************************************************************************************************************/
/* perform integrity check when it is necessary */
#if ( OS_MAIN_C_VERSION != 0x0100U )
#error "Incorrect version of header file used in filename.h!!!"
#endif


/*********************************************************************************************************************************/
/*                                          M A C R O   D E F I N I T I O N S                                         */
/*********************************************************************************************************************************/


/**********************************************************************************************************************/
/*                                          T Y P E   D E F I N I T I O N S                                           */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                    L O C A L   V A R I A B L E S   D E F I N I T I O N S                           */
/**********************************************************************************************************************/

/* Round robin list */
rr rr_list[RR_COUNT]= {RR_LIST};

/*Time task list */
task tasks[TT_COUNT] = {TT_LIST};

/* Init Task list */
init_list init_tasks[INIT_LIST_COUNT] = {INIT_LIST};

/*Variable to hold tick count */
tick_t mills= 0x00U;

/*Timer flag*/
static tt_flag_t TimerFlag = 0x00U;

/*Task counter variable */
tt_count_t task_count = 0x00;

/*Tick Timer rate */
static const unsigned long tasksPeriodGCD = 0x01; // Timer tick rate

/*Variable to hold the current context of the Scheduler*/
static UINT8 context;


/**********************************************************************************************************************/
/*                                     G L O B A L   V A R I A B L E S   D E F I N I T I O N S                        */
/**********************************************************************************************************************/



/**********************************************************************************************************************/
/*                                          C O N S T A N T S   D E F I N I T I O N S                                 */
/**********************************************************************************************************************/



/**********************************************************************************************************************/
/*                                    P R I V A T E    F U N C T I O N S   D E C L A R A T I O N                      */
/**********************************************************************************************************************/

/******************************************************************************************/
/*Function Name : Execute_List                                                            */
/*input         : list -  pointer to list of task need to be executed in round robin      */
/*                mode -  CPU context mode in which functions can be executed             */
/*              : count - Numbers of task to be executed in round robin                   */
/*Description   : This function executes the list of task provided by user in the round   */
/*              : robin list with respective to their context value                       */
/*Return Type   : None                                                                    */
/*Invocation    : Scheduler                                                               */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/

static void Execute_List(rr * list, UINT8 mode, UINT8 count);

/********************************************************************************************/
/*Function Name : Execute_InitList                                                          */
/*input         : list -  pointer to list of task need to be executed during initialization */
/*Description   : This function executes the list of task provided by user in the Init      */
/*              : list during the initialization time                                       */
/*Return Type   : None                                                                      */
/*Invocation    : Scheduler                                                                 */
/*Note: This is Local function to this module and should not used outside of this file.     */
/******************************************************************************************/

static void Execute_InitList(init_list * list);

/******************************************************************************************/
/*Function Name : Hardware_Init                                                            */
/*input         : None                                                                     */
/*Description   : Initializes all the GPIO pins and Timer peripheral used in the scheduler */                                                                         */
/*Return Type   : None                                                                     */
/*                                                                                         */
/*                                                                                         */
/*Invocation    : Scheduler                                                                         */
/*Note: This is Local function to this module and should not used outside of this file.    */
/******************************************************************************************/

static void Hardware_Init(void);


/**********************************************************************************************************************/
/*                                   P U B L I C   F U N C T I O N S   D E F I N I T I O N S                          */
/**********************************************************************************************************************/

/******************************************************************************************/
/*Function Name : app_user_init                                                           */
/*input         : None                                                                    */
/*Description   : This function initializes all peripheral related to application and     */
/*              : the scheduler                                                           */
/*Return Type   : None                                                                    */
/******************************************************************************************/


void app_user_init(void)
{


    /* Execute all the initialization task */
    Execute_InitList(init_tasks);

    /*Initialize the hardware*/
    Hardware_Init();

    /*Set the OS context to Boot */
    context = OS_C_BOOT;

}

/******************************************************************************************/
/*Function Name : app_service_watchdog                                                    */
/*input         : None                                                                    */
/*Description   : This function is used to service the watch dog timer                    */
/*Return Type   : None                                                                    */
/*Invocation    : Scheduler / Application                                                 */
/******************************************************************************************/

void app_service_watchdog(void)
{

}

/******************************************************************************************/
/*Function Name : app_schedule                                                            */
/*input         : None                                                                    */
/*Description   : This function schedules the task provided in the round robin list       */
/*              : with respect to their context values                                    */
/*Return Type   : None                                                                    */
/*Invocation    :                                                                         */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/
void  app_schedule(void)
{

      // Heart of the scheduler code
      if (TimerFlag == 0x01U) {
      }
           Schedule();

}

/******************************************************************************************/
/*Function Name : appRtiIsr                                                               */
/*input         : None                                                                    */
/*Description   : This is ISR function for the timer used for generating system tick and  */
/*              : scheduling time task                                                    */
/*Return Type   : None                                                                    */
/*Invocation    : Scheduler/Driver                                                                         */
/******************************************************************************************/

void appRtiIsr(void)
{
      /* count the  elapsed time in millisecond from the start of scheduler */
      mills = mills + tasksPeriodGCD;

      /*Check the timerflag set */
      if (TimerFlag)
      {
          //printf("Timer ticked before task processing done.\n");
          // debug("WE\n\r");
      }
      else
      {
          TimerFlag = OS_TIMER_FLAG_SET;
          for (task_count=0x00U; task_count < TT_COUNT; ++task_count)
          {
              /* Check whether the time task is ready to execute */
              if (tasks[task_count].elapsedTime >= tasks[task_count].period)
              {
                  /* Execute time task */
                  tasks[task_count].TickFct();
                  /*Clear the elapsed time */
                  tasks[task_count].elapsedTime = OS_CLEAR_ELAPSED_TIME;

              }

              /* Increment the elapsed time counter */
              tasks[task_count].elapsedTime += tasksPeriodGCD;
           }

          /* Clear the Timer Flag */
          TimerFlag = OS_TIMER_FLAG_CLEAR;
       }
}

/******************************************************************************************/
/*Function Name : getContext                                                              */
/*input         : None                                                                    */
/*Description   : this function returns the Scheduler context                             */
/*Return Type   : unsigned char context value                                             */
/*Invocation    : Application/Scheduler                                                   */
/******************************************************************************************/

UINT8 getContext(void)
{

    return context;
}

/******************************************************************************************/
/*Function Name : setContext                                                              */
/*input         : new_context - new context value need to be set                          */
/*Description   : this function sets the Scheduler context                                */
/*Return Type   : None                                                                    */
/*Invocation    : Application/Scheduler                                                   */
/******************************************************************************************/

void setContext(UINT8 new_context)
{

     context = new_context;
}

/******************************************************************************************/
/*Function Name : Schedule                                                                */
/*input         : None                                                                    */
/*Description   : This function is executes the task listed in a user specified round     */
/*                list in config file                                                     */
/*Return Type   : None                                                                        */
/*Invocation    : Scheduler                                                                        */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/

void Schedule(void)
{
    Execute_List(rr_list,0,RR_COUNT);
}

/******************************************************************************************/
/*Function Name : Execute_List                                                            */
/*input         : list -  pointer to list of task need to be executed in round robin      */
/*                mode -  CPU context mode in which functions can be executed             */
/*              : count - Numbers of task to be executed in round robin                   */
/*Description   : This function executes the list of task provided by user in the round   */
/*              : robin list with respective to their context value                       */
/*Return Type   : None                                                                    */
/*Invocation    : Scheduler                                                               */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/

void Execute_List(rr * list, UINT8 mode, UINT8 count)
{
    UINT8 i;
    UINT8 l_context;

    for (i = 0; i < count; i++)
    {
        l_context = getContext();
        if ((list[i].context & l_context) == l_context  )
        {
            list[i].rr_fn();
        }

    }
}

void Hardware_Init(void)
{

#if (Micro_Platform == TMS_570LS1224)

    rtiInit();
    /* Enable RTI Compare 0 interrupt notification */
    rtiEnableNotification(rtiNOTIFICATION_COMPARE0);

    /* Enable IRQ - Clear I flag in CPS register */
    /* Note: This is usually done by the OS or in an svc dispatcher */
    _enable_IRQ();
    /* Start RTI Counter Block 0 */
    rtiStartCounter(rtiCOUNTER_BLOCK0);
    /**/

#elif

#error Please select the correct Microcontroller and platform variant

#endif


}
/******************************************************************************************/
/*Function Name :                                                                         */
/*input         :                                                                         */
/*Length        :                                                                         */
/*buffer        :                                                                         */
/*Description   :                                                                         */
/*Return Type   :                                                                         */
/*                                                                                        */
/*                                                                                        */
/*Invocation    :                                                                         */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/

void Execute_InitList(init_list * list)
{

    UINT8 i;
    i=0;

    while ( list[i].InitFct != 0 )
    {
        list[i].InitFct();
        i++;
    }
}

/******************************************************************************************/
/*Function Name :                                                                         */
/*input         :                                                                         */
/*Length        :                                                                         */
/*buffer        :                                                                         */
/*Description   :                                                                         */
/*Return Type   :                                                                         */
/*                                                                                        */
/*                                                                                        */
/*Invocation    :                                                                         */
/*Note: This is Local function to this module and should not used outside of this file.   */
/******************************************************************************************/

tick_t millis(void)
{
    return mills;
}


/**********************************************************************************************************************/
/*REVISION NUMBER      : 1.0                                                                                      */
/*REVISION DATE        : 11 - 06 - 2017                                                                           */
/*CREATED / REVISED BY : BALAJI PANNEERSELVAM                                                                     */
/*DESCRIPTION          : Initial version                                                                          */
/*--------------------------------------------------------------------------------------------------------------------*/

/***********************************************************************************************************************/



