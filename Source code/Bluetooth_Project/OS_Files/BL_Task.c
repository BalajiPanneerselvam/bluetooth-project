#ifndef BL_TASK_C
#define BL_TASK_C
/***********************************************************************************************************************/
/* DISCLAIMER                                                                                                          */
/*                                                                                                                     */
/* Copyright (C) 2011, 2016 Niyata Infotech Ptv. Ltd. All rights reserved.                                             */
/***********************************************************************************************************************/
/*                                               F I L E  D E S C R I P T I O N                                        */
/*---------------------------------------------------------------------------------------------------------------------*/
/*File name                       :Bl_Task.c                                                                           */
/*Version                         :V1.0                                                                                */
/*Micros supported                :TMS570LS1224                                                                        */
/*Compilers supported             :GCC(CCS IDE)                                                                        */
/*Platforms supported             :Hercules                                                                            */
/*Description                     :Bluetooth task                                                                      */
/*                                :                                                                                    */
/*                                :                                                                                    */
/*----------------------------------------------------------------------------------------------------------------------*/
/**********************************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/**********************************************************************************************************************/
/*                                                 F I L E  V E R S I O N                                             */
/**********************************************************************************************************************/
#define BL_TASK_C_VERSION  0x0100    /* BCD values - MSB & LSB represent major & minor versions respectively \
                                        00.00 to 99.99 */

/**********************************************************************************************************************/
/*                                               I N C L U D E   F I L E S                                            */
/**********************************************************************************************************************/
#include "Bl_Task.h"
#include "sci.h"
#include "gio.h"
/**********************************************************************************************************************/
/*                                      V E R S I O N   I N T E G R I T Y  C H E C K                                  */
/**********************************************************************************************************************/

#if ( BL_TASK_C_VERSION != 0x0100 )
    #error "Incorrect version of Bl_Task.h file !!!"
#endif

/**********************************************************************************************************************/
/*                                          M A C R O   D E F I N I T I O N S                                         */
/**********************************************************************************************************************/



/**********************************************************************************************************************/
/*                                          T Y P E   D E F I N I T I O N S                                           */
/**********************************************************************************************************************/

const uint8_t Bluetooth_App_Firmare_Version[BL_TASK_FIRMWARE_V_LENGTH] = {'B','L','A','P','P','1','.','0','\r','\n'};




/**********************************************************************************************************************/
/*                                    L O C A L   V A R I A B L E S   D E F I N I T I O N S                           */
/**********************************************************************************************************************/
struct BL_App_Data
{
    uint8_t Rx_Command;
    BL_States_t BL_Current_State;
    BL_Access_flg_t BL_Access_Flag;


}BL_App_Data_t;


/**********************************************************************************************************************/
/*                                     G L O B A L   V A R I A B L E S   D E F I N I T I O N S                        */
/**********************************************************************************************************************/



/**********************************************************************************************************************/
/*                                          C O N S T A N T S   D E F I N I T I O N S                                 */
/**********************************************************************************************************************/



/**********************************************************************************************************************/
/*                                    P R I V A T E    F U N C T I O N S   D E C L A R A T I O N                      */
/**********************************************************************************************************************/




/**********************************************************************************************************************/
/*                                   P U B L I C   F U N C T I O N S   D E F I N I T I O N S                          */
/**********************************************************************************************************************/

void BL_Task_init(void)
{

    BL_App_Data_t.BL_Current_State = BL_Idle_State;

}
/******************************************************************************************/
/*Function Name : BL_Bg_Task                                                                        */
/*input         : None                                                                        */
/*Description   : This function checks the  */
/*                appropriate action */
/*Return Type   : None                                                                         */
/*                                                                                        */
/*                                                                                        */
/*Invocation    : Driver                                                                        */
/******************************************************************************************/

void BL_Bg_Task(void)
{

    switch(BL_App_Data_t.BL_Current_State)
   {

       case BL_Connect_State:

           BL_Task_Send_Response(BL_App_Data_t.Rx_Command);
           BL_App_Data_t.BL_Current_State = BL_Idle_State;
       break;

       case BL_Enable_State:

           BL_App_Data_t.BL_Access_Flag = BL_PERMIT;
           BL_Task_Send_Response(BL_App_Data_t.Rx_Command);
           BL_App_Data_t.BL_Current_State = BL_Idle_State;
       break;

       case BL_Disable_State:

           BL_App_Data_t.BL_Access_Flag = BL_DENIED;
           BL_Task_Send_Response(BL_App_Data_t.Rx_Command);
           BL_App_Data_t.BL_Current_State = BL_Idle_State;
       break;

       case BL_Request_State:

           BL_Task_Send_Response(BL_App_Data_t.Rx_Command);
           BL_App_Data_t.BL_Current_State = BL_Idle_State;
       break;



       default:


       break;

   }

}

/******************************************************************************************/
/*Function Name : BL_Callback                                                                        */
/*input         : None                                                                        */
/*Description   : This function is a callback function for BL_task and will receive the incoming data from uart and takes */
/*                appropriate action */
/*Return Type   : None                                                                         */
/*                                                                                        */
/*                                                                                        */
/*Invocation    : Driver                                                                        */
/******************************************************************************************/

void BL_Callback(uint8_t Rx_data)
{

    /*Checks the received data*/
    switch(Rx_data)
    {

    /* Command to Connect */
    case BL_CONNECT:

        BL_App_Data_t.BL_Current_State = BL_Connect_State;
        BL_App_Data_t.Rx_Command = 0x00U;
    break;

    /*Command to Enable BL*/
    case BL_ENABLE:

        BL_App_Data_t.BL_Current_State = BL_Enable_State;
        BL_App_Data_t.Rx_Command = 0x01U;
    break;

    /*Command to Disable BL*/
    case BL_DISABLE:

        BL_App_Data_t.BL_Current_State = BL_Disable_State;
        BL_App_Data_t.Rx_Command = 0x02U;
    break;

    /*Command to Request BL version*/
    case BL_VERSION_REQUEST:

        BL_App_Data_t.BL_Current_State = BL_Request_State;
        BL_App_Data_t.Rx_Command = 0x03U;

    break;

    /*Command to Request Serial Number version*/
    case BL_SERINAL_NUMBER_REQUEST:

        BL_App_Data_t.BL_Current_State = BL_Request_State;
        BL_App_Data_t.Rx_Command = 0x04U;
    break;

    /*Invalid Commands*/
    default:


    break;


    }


}

void BL_Task_Send_Response(uint8_t rx_command)
{
    uint8_t Rx_data[0x0BU];
    uint8_t i;

    Rx_data[0] = Bluetooth_App_Command_Table[rx_command][0x00U];
    for(i=1;i<=Rx_data[0];i++)
    {
        Rx_data[i] = Bluetooth_App_Command_Table[rx_command][i];

        if(i == Rx_data[0])
        {
            Rx_data[i+1] = 0x0D;
        }
        else
        {

        }
     }


    sciSend(sciREG,Rx_data[0]+2, Rx_data);


}

void BL_Switch_Task(void)
{

     if(BL_App_Data_t.BL_Access_Flag == BL_PERMIT)
     {

         gioToggleBit(gioPORTB,1);

     }
     else
     {


     }


}
/**********************************************************************************************************************/
/*                                    P R I V A T E   F U N C T I O N S   D E F I N I T I O N S                       */
/**********************************************************************************************************************/





/**********************************************************************************************************************/
/*                                              E N D   O F   S O F T W A R E                                         */
/**********************************************************************************************************************/
#ifdef __cplusplus
}
#endif
/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                              R E V I S I O N   H I S T O R Y                                       */
/*                                                                                                                    */
/***********************************************************************************************************************
    REVISION NUMBER      : 1
    REVISION DATE        : 11 - 07 - 2017
    CREATED / REVISED BY : Balaji
    DESCRIPTION          : Initial version
------------------------------------------------------------------------------------------------------------------------

***********************************************************************************************************************/
#endif
