/*
 * Sample_Task.h
 *
 *  Created on: Nov 6, 2017
 *      Author: Roop
 */

#ifndef OS_FILES_SAMPLE_TASK_H_
#define OS_FILES_SAMPLE_TASK_H_

void Sample_rr_task(void);
void Sample_Init(void);
void Sample_tt_task1(void);
void Sample_tt_task2(void);
void Sample_tt_task3(void);
void Sample_bltask(unsigned char Rx_data);
void Sample_BlService(unsigned char val);
void Sample_BlRequest(void);

#endif /* OS_FILES_SAMPLE_TASK_H_ */
